﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AStudyApp_Backend.Startup))]
namespace AStudyApp_Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
